Source: kpipewire
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Patrick Franz <deltaone@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-pkgkde-symbolshelper,
               dh-sequence-qmldeps,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.10.0~),
               gettext,
               libavcodec-dev,
               libavfilter-dev,
               libavformat-dev,
               libavutil-dev,
               libdrm-dev (>= 2.4.62~),
               libegl-dev,
               libepoxy-dev (>= 1.3~),
               libgbm-dev,
               libkf6coreaddons-dev (>= 6.2.0~),
               libkf6i18n-dev (>= 6.2.0~),
               libpipewire-0.3-dev,
               libswscale-dev,
               libva-dev,
               libwayland-dev,
               libxkbcommon-dev,
               pipewire-bin,
               pkgconf,
               plasma-wayland-protocols,
               qt6-base-dev (>= 6.6.0+dfsg~),
               qt6-base-private-dev (>= 6.6.0+dfsg~),
               qt6-declarative-dev (>= 6.6.0+dfsg~),
               qt6-declarative-private-dev (>= 6.6.0+dfsg~),
               qt6-wayland-dev (>= 6.6.0~),
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/plasma/kpipewire
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kpipewire
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kpipewire.git
Rules-Requires-Root: no

Package: libkpipewire-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: KDE's Pipewire libraries - data files
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the library data files.

Package: libkpipewire-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Depends: libkpipewire6 (= ${binary:Version}),
         libkpipewiredmabuf6 (= ${binary:Version}),
         libkpipewirerecord6 (= ${binary:Version}),
         libpipewire-0.3-dev,
         qml6-module-org-kde-pipewire (= ${binary:Version}),
         qt6-base-dev (>= 6.6.0+dfsg~),
         ${misc:Depends},
Description: KDE's Pipewire libraries - development files
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the library development files.

Package: libkpipewire6
Architecture: linux-any
Multi-Arch: same
Depends: libkpipewire-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: KDE's Pipewire libraries - libkpipewire6 library
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the kpipewire main library.

Package: libkpipewiredmabuf6
Architecture: linux-any
Multi-Arch: same
Depends: libkpipewire-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: KDE's Pipewire libraries - libkpipewiredmabuf6
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the kpipewiredmabuf library.

Package: libkpipewirerecord6
Architecture: linux-any
Multi-Arch: same
Depends: libkpipewire-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: KDE's Pipewire libraries - libkpipewirerecord6
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the kpipewirerecord library.

Package: qml6-module-org-kde-pipewire
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends}, ${qml6:Depends}, ${shlibs:Depends},
Description: KDE's Pipewire libraries - QML module
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the QtQuick module.
