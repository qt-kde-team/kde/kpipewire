Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kpipewire
Source: https://invent.kde.org/plasma/kpipewire
Upstream-Contact: plasma-devel@kde.org

Files: *
Copyright: 2020-2023, Aleix Pol Gonzalez <aleixpol@kde.org>
           2023 Arjen Hiemstra <ahiemstra@heimr.nl>
           Jan Grulich <jgrulich@redhat.com>
           2023 Marco Martin <mart@kde.org>
           2023 Noah Davis <noahadvs@gmail.com>
           2018-2020, Red Hat Inc
License: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

Files: src/monitors/autotests/alarm-clock-elapsed.oga
       src/monitors/autotests/mediamonitortest.cpp
       src/monitors/autotests/mediamonitortest.qml
       src/monitors/mediamonitor.cpp
       src/monitors/mediamonitor.h
Copyright: 2023 Fushan Wen <qydwhotmail@gmail.com>
           2023 Collabora Ltd.
License: GPL-2.0-or-later

Files: cmake/FindLibdrm.cmake
       src/monitors/CMakeLists.txt
       src/monitors/autotests/CMakeLists.txt
Copyright: 2014, Alex Merry <alex.merry@kde.org>
           2023 Fushan Wen <qydwhotmail@gmail.com>
           2014, Martin Gräßlin <mgraesslin@kde.org>
License: BSD-3-clause

Files: src/dmabufhandler.cpp
Copyright: 2022 Aleix Pol i Gonzalez <aleixpol@kde.org>
License: Apache-2.0

Files: .gitlab-ci.yml
       .kde-ci.yml
Copyright: None
License: CC0-1.0

Files: debian/*
Copyright: 2022-2024, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2022, Jonathan Esk-Riddell <jr@jriddell.org>
           2022, Rik Mills <rikmills@kde.org>
License: GPL-2.0-or-later

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: LGPL-2.1-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version
 2.1 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1’.

License: LGPL-3.0-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version
 3 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in
 `/usr/share/common-licenses/LGPL-3’.

License: LicenseRef-KDE-Accepted-LGPL
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 license or (at your option) any later version that is accepted by
 the membership of KDE e.V. (or its successor approved by the
 membership of KDE e.V.), which shall act as a proxy as defined in
 Section 6 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

License: CC0-1.0
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS DOCUMENT OR
 THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR
 DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator and
 subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 On Debian systems, the complete text of the Creative Commons Zero v1.0 Universal
 license can be found in "/usr/share/common-licenses/CC0-1.0".

License: Apache-2.0
  Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License 2.0 can
 be found in "/usr/share/common-licenses/Apache-2.0"
